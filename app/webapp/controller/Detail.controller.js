sap.ui.define([
	"TraceReport/controller/BaseController",
	"sap/m/Dialog",
	"sap/m/MessageToast"
], function(BaseController, Dialog, MessageToast) {
	"use strict";

	let oDialog; 
	let index;

	return BaseController.extend("TraceReport.controller.Detail", {

		onInit: function() {
			this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);
			oDialog = sap.ui.xmlfragment(this.getView().getId(), "TraceReport.view.DialogTicket", this);
			this.getView().addDependent(oDialog);
            this._VIEW = this.getView();
		},

		print: function() {
			var oModel = this._VIEW.getModel("data"); 
            oModel.setProperty("/MODE", "HideMode"); 
            console.log("oModel", oModel);
            //var oSplitContainer = sap.ui.getCore().byId("splitApp");
            //console.log(oSplitContainer);
            //oSplitContainer.setMode(sap.m.SplitAppMode.StretchCompressMode);
            //if(oSplitContainer.isMasterShown()){
                //oSplitContainer.setMode(sap.m.SplitAppMode.HideMode);
			//window.print();
		},

		_onRouteMatched: function(oEvent) {
			//Obtengo id (posicion del ticket seleccionado) de la vista Master
			var oArguments = oEvent.getParameter("arguments");
			index = oArguments.id;
			
			var oModel = this._VIEW.getModel("data"); 
			
			//Obtengo ID del ticket segun la posicion del ticket seleccionado en el Master
			var selectedTicketId = oModel.getProperty("/tickets/"+ index + "/Z_TICK");
            oModel.setProperty("/detail", {});
            //console.log("selectedTicket", selectedTicketId);
			//Llamada ajax para listar detalle del ticket seleccionado
			const that = this;
            $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "http://localhost:3000/bascule/Z_WGHT_P/"+selectedTicketId,
                dataType: "json",
                async: false,
                success: function(data) {
                    if(data.length > 0) {

                        oModel.setProperty("/detail/", data);
                        //SE RECORRE POSICIONES PARA RESPECTIVAS VALIDACIONES
                        for(var i=0; i < data.length; i++) {

                            var status = data[i].Z_STATUS;
                            oModel.setProperty("/detail/"+i+"/Z_STATUS_D/", that.status(status));
                            
                            //OBTENGO DESCRIPCION DEL MATERIAL SEGUN ID DEL MATERIAL
                            var matId = data[i].MATNR;
                            $.ajax({
                                type: "GET",
                                contentType: "application/json",
                                url: "http://localhost:3000/bascule/MAKT/" + matId,
                                dataType: "json",
                                async: false,
                                success: function(data2) {
                                    if(data2.length > 0) {
                                        oModel.setProperty("/detail/"+i+"/MAKTX/", data2[0].MAKTX);
                                    }
                                },
                                error: function(request, status, error) {
                                    MessageToast.show(status + " : " + error);
                                    console.log("Read failed", error);
                                }
                            });

                            var documento = data[i].EBELN;
                            console.log("documento", documento)
                            $.ajax({
                                type: "GET",
                                contentType: "application/json",
                                url: "http://localhost:3000/bascule/EKKO/" + documento,
                                dataType: "json",
                                async: false,
                                success: function(dataDoc) {
                                    console.log("dataDoc", dataDoc);
                                    if(data.length > 0) {
                                        oModel.setProperty("/detail/"+i+"/PROV_ID/", dataDoc[0].LIFNR);
                                        var provId = dataDoc[0].LIFNR; 
                                        $.ajax({
                                            type: "GET",
                                            contentType: "application/json",
                                            url: "http://localhost:3000/bascule/LFA1/" + provId,
                                            dataType: "json",
                                            async: false,
                                            success: function(dataProv) {
                                                console.log("dataProv", dataProv)
                                                if(dataProv.length > 0) {
                                                    oModel.setProperty("/detail/"+i+"/PROV_NAME/", dataProv[0].NAME1);
                                                }
                                            },
                                            error: function(request, status, error) {
                                                MessageToast.show(status + " : " + error);
                                                console.log("Read failed", error);
                                            }
                                         });
                                    }
                                },
                                error: function(request, status, error) {
                                    MessageToast.show(status + " : " + error);
                                    console.log("Read failed", error);
                                }
                            });

                        }

                        //SE GUARDA DATOS EN EL HEADER DEL MODELO
                        oModel.setProperty("/header", oModel.getProperty("/tickets/" + index));
                        
                        //ACTUALIZO COMPARTIMIENTOS
                        //that.onReloadCompartments(selectedTicketId);

                    } else { MessageToast.show("El Ticket no tiene Posiciones", {duration:3000, autoClose: false}); }                       
                },
                error: function(request, status, error) {
                    MessageToast.show(status + " : " + error);
                    console.log("Read failed", error);
                }
            });
            console.log(oModel.oData); 
		},
		
		//DIALOG QUE GESTIONA EL GUARDADO Y VALIDA VALORES DE ENTRADA.
		onSave: function(oEvent){
			this.print();
		},

		//EVENTO QUE CIERRA EL DIALOG DE CONFIRMACION
		onCloseDialog: function (oEvent) {
			oDialog.close();
		},

		//EVENTO QUE ACTUALIZA ESTADO DEL TICKET EN SU CABECERA Y DETALLE
		onSaveDialog: function(oEvent) {
			var item = this.getView().byId("op_select").getSelectedKey();
		
			var that = this;
			//console.log("Operacion select", item);
			//console.log("INDEX", index);
			var oModel = this.getView().getModel("data");
			var selectedTicketId = oModel.getProperty("/tickets/"+ index + "/Z_TICK");
			//console.log("Detail - ID_TICKET", selectedTicketId);
			var statust = oModel.getProperty("/header/Z_STATUS");
			//console.log("Estatus - Ticket", statust);
			var datos = { 'Z_STATUS' : item };
			
			//SE ACTUALIZA STATUS DE LA CABECERA DEL TICKET
			$.ajax({
				type: "PUT",						
				url: "http://localhost:3000/bascule/Z_WGHT_H/" + selectedTicketId,					
				data  : datos,						
				dataType: "json",
				async: false,						
				success: function(response) {
					//SE ACTUALIZA STATUS DE LAS POSICIONES DEL TICKET.
					$.ajax({
						type: "PUT",						
						url: "http://localhost:3000/bascule/Z_WGHT_P/update/" + selectedTicketId + "/" + statust,
						//url: "http://localhost:3000/bascule/Z_WGHT_P/" + selectedTicketId,							
						data  : datos,						
						dataType: "json",
						async : false,						
						success: function(response) {	
							MessageToast.show("Ticket "+selectedTicketId+" Actualizado", {duration:3000, autoClose: false});	
						},						
						error: function(err) {							
							MessageToast.show("Detalle del Ticket "+selectedTicketId+" no pudo ser actualizado");	
						}
					});						
				},						
				error: function(err) {							
					MessageToast.show("Ticket "+selectedTicketId+" no pudo ser actualizado");	
				}
			});

			this.onUpdateMaster();
			oDialog.close();
			
		},

		//Actualizo listado de tickets en el Master
		onUpdateMaster: function(oEvent) {
			
            setTimeout(function(){
                location.href = 'http://localhost:3000/apps/Unlocked/app/webapp/'
            }, 2000);				
		},

        status : function(status) {
              var result;
              switch (status) {
                case "1":
                    result = "Primera Pesada";
                    break;
                case "3":
                    result = "Rechazado";
                    break;
                case "4":
                    result = "Descarga";
                    break;
                case "5":
                    result = "Bloqueado";
                    break;
                case "7":
                    result = "Desbloqueado";
                    break;
                case "8":
                    result = "No Aprobado";
                    break;
                case "11":
                    result = "Carga";
                    break;  
              }
              return result;
        },

	});
});